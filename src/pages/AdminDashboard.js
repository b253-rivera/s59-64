import { useEffect, useState } from 'react';
import ProductCard from '../components/ProductCard';
import {Form, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';


export default function AdminDashboard(){

    const [ products, setProducts ] = useState([]);
    const [ name, setName ] = useState('');
    const [ description, setDescription ] = useState('');
    const [ price, setPrice ] = useState(0);

    useEffect(() => {

        fetch(`http://localhost:4000/products/hardware`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
        })
        .then(res => res.json())
        .then(data => {
            setProducts(data);
        })
    }, []);

    const handleSubmit = (event) => {

        event.preventDefault();

        const formData = new FormData(event.target);
        const name = formData.get('name');
        const description = formData.get('description');
        const price = formData.get('price');


        const productData = {
            name,
            description,
            price
        };

        fetch(`http://localhost:4000/products/createproduct`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem(' token ')}`
            },
            body: JSON.stringify(productData)
        })

        .then(res => res.json())
        .then(data => {
            console.log(data);
            setProducts([...products, data]);
        })
        .catch(error => console.error(error));
    };
    console.log(products);
    return (
        <>
            <div className="text-center">
            <h1>Create Product</h1>
            <Link className="btn btn-primary" to={'/products/admin/create'}>Create</Link>
            </div>
{/*            <h1>Update Product</h1>
            <Link className="btn btn-primary" to={'/products/admin/update'}>Update</Link>*/}
            <h1>All Products</h1>
            { products.map(product => (

                <ProductCard key={ product._id } product={product} />
            ))}
        </>



    )
}
