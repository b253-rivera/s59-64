import Banner from '../components/Banner';
import Carousel from 'react-bootstrap/Carousel';

import Highlights from '../components/Highlights';

export default function Home() {

	const data = {
	    title: "NetCodexPH",
	    content: "Your number 1 shop for computer parts",
	    destination: "/products",
	    label: "Buy now!"
	}

	return (
		<>
			<Banner data={ data } />
			<Highlights />

		</>
	)
}