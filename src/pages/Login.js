import { useState, useEffect, useContext } from 'react';

import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

import UserContext from '../UserContext';

export default function Login() {

  // Allows us to consume the User context object and it's properties to use for user validation 
  const { user, setUser } = useContext(UserContext);

  // State hooks to store the values of the input fields
  const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(true);

  function authenticate(e) {

        // Prevents page redirection via form submission
        e.preventDefault();

        // Process a fetch request to the corresponding backend API
        // The header information "Content-Type" is used to specify that the information being sent to the backend will be sent in the form of JSON
        // The fetch request will communicate with our backend application providing it with a stringified JSON
        // Convert the information retrieved from the backend into a JavaScript object using the ".then(res => res.json())"
        // Capture the converted data and using the ".then(data => {})"
        // Syntax
            // fetch('url', {options})
            // .then(res => res.json())
            // .then(data => {})
        fetch('http://localhost:4000/users/login',
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            email: email,
            password: password
          })
        })
        .then(res => res.json())
        .then(data => {
          console.log(data)

 
          if(typeof data.access !== "undefined"){


            localStorage.setItem('token', data.access);
            retrieveUserDetails(data.access);

            Swal.fire({
              title: "Login Successful",
              icon: "success",
              text: "Welcome to NetCodexPh!"
            });
          } else {

            Swal.fire({
              title: "Authentication Failed",
              icon: "error",
              text: "Check your login credentials and try again."
            });
          }
        });

        setEmail('');
        setPassword('');

    }

    const retrieveUserDetails = (token) => {


      fetch('http://localhost:4000/users/details', {
        headers: {
          Authorization: `Bearer ${ token }`
        }
      })
      .then(res => res.json())
      .then(data => {

        console.log(data);

        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      })
    };



  useEffect(() => {


        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [email, password]);

    return (

      (user.id !== null) ?
        <Navigate to="/"/>
      :
        <Form onSubmit={e => authenticate(e)}>
            <Form.Group className="mb-3" controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
              onChange={(e) => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password}
              onChange={(e) => setPassword(e.target.value)}
                    required
                />
            </Form.Group>

            { isActive ? 
                <Button variant="success" type="submit" id="submitBtn">
                    Submit
                </Button>
                : 
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
            }

        </Form>
  )
}