import React from 'react';
import { Button } from 'react-bootstrap';
import { useState } from 'react';

function AdminArchive({ productId, onArchive }) {

  const [isActive, setIsActive] = useState(true);

  // Function to handle archiving a product
  const handleArchive = async () => {
    // Send API call to backend server to archive a product
    await fetch(`http://localhost:4000/products/:productId/archive`, {
      method: 'PUT',
      headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem(' token ')}`
            },
            body: JSON.stringify({
              isActive: isActive
            })
    });

    // Notify parent component that the product has been archived
    onArchive(productId);
  };

  return (
    <Button variant="danger" onClick={handleArchive}>
      Archive
    </Button>
  );
}

export default AdminArchive;