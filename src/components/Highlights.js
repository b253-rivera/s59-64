import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights() {
	return (
		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				      <Card.Body className="text-center">
				        <Card.Title>
				        	<h2>Huge Discount</h2>
				        </Card.Title>
				        <Card.Text>
				          We make building a PC Computer so simple
				        </Card.Text>
				      </Card.Body>
				    </Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				      <Card.Body className="text-center">
				        <Card.Title>
				        	<h2>PC Packages</h2>
				        </Card.Title>
				        <Card.Text>
				          Premium quality PC packages with a lot of discounts.
				        </Card.Text>
				      </Card.Body>
				    </Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				      <Card.Body className="text-center">
				        <Card.Title>
				        	<h2>Nationwide Shipping</h2>
				        </Card.Title>
				        <Card.Text>
				          Cash on delivery, Free Shipping and Same day delivery
				        </Card.Text>
				      </Card.Body>
				    </Card>
			</Col>
			<Col xs={12} md={6}>
				<Card className="cardHighlight p-3">
				      <Card.Body className="text-center">
				        <Card.Title>
				        	<h2>Seasonal GPU</h2>
				        </Card.Title>
				        <Card.Text>
				          Cash on delivery, Free Shipping and Same day delivery
				        </Card.Text>
				      </Card.Body>
				    </Card>
			</Col>
			<Col xs={12} md={6}>
				<Card className="cardHighlight p-3">
				      <Card.Body className="text-center">
				        <Card.Title>
				        	<h2>Gigabyte</h2>
				        </Card.Title>
				        <Card.Text>
				          Cash on delivery, Free Shipping and Same day delivery
				        </Card.Text>
				      </Card.Body>
				    </Card>
			</Col>
			<Col xs={12} md={12}>
				<Card className="cardHighlight p-3">
				      <Card.Body className="text-center">
				        <Card.Title>
				        	<h2>MSI</h2>
				        </Card.Title>
				        <Card.Text>
				          Cash on delivery, Free Shipping and Same day delivery
				        </Card.Text>
				      </Card.Body>
				    </Card>
			</Col>

		</Row>

	)
}